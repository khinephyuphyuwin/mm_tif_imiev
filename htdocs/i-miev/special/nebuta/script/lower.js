var youtube_list = new Array();

//////////////////////////////////////////////////////////////////////////////////////////////
$(function(){
	//一度非表示
	$("#contents-body-inner").css({"opacity": "0"});
	$("#background-light").css({"opacity": "0"});
	$("#background-shadow").css({"opacity": "0"});
	$("html").css({"overflow": "hidden"});
	$("#popcontents-footer").css({"visibility": "visible"});
	
	//-------------------------------------------------
	var skip = ( document.URL.indexOf('lineup.html') != -1 ) ? true : (location.hash == "#skip") ? true : false;
	
	
	//-------------------------------------------------
	$(".report-youtube").each(function(){
		youtube_list.push($(this));
	});
			
			
	//------------------------------------------------------
	if( skip )
	{
		$("#main-visual").imagesLoaded(function(){
			play(skip);
		});
	}else {
		play(skip);
	}
	
	//IE6でpngを表示
	//-------------------------------------------------
	if(jQuery.browser.msie && parseInt(jQuery.browser.version) == 6){
			$('html').css({
				'background-image':'url(null)',
				'background-attachment':'fixed'
			});
			DD_belatedPNG.fix('#imiev-car img');
			DD_belatedPNG.fix('#log');
			DD_belatedPNG.fix('#mixi a');
			DD_belatedPNG.fix('#goto-top a');
			DD_belatedPNG.fix('#background-shadow');
			DD_belatedPNG.fix('#background-cover');
			
			$('body').css({
				'background-image':'url(null)',
				'background-attachment':'fixed'
			});
			var el = $('#popcontents-footer').css('position','absolute')[0];
			el.style.setExpression('top',
				'eval($(window).height() \+ $(window).scrollTop() - 38 - 24)'
			);
			var el2 = $('#footerpop').css('position','absolute')[0];
			el2.style.setExpression('top',
				'eval($(window).height() \+ $(window).scrollTop() - 24)'
			);
	}else if( jQuery.browser.msie && parseInt(jQuery.browser.version) == 7 )
	{
		$("#background-cover").remove();
		$("#background-light").remove();
		$("#background-shadow").remove();
	}
	
	//iPhone/iPadでfixedを実装
	//-------------------------------------------------
	if(navigator.userAgent.indexOf('iPhone') != -1 || navigator.userAgent.indexOf('iPad') != -1){
		$("#popcontents-footer").css( {"position": "absolute"} );
		$("#footerpop").css( {"position": "absolute"} );
		
		var id = setInterval(tmp, 10);
		
		document.addEventListener("touchmove", touchHandler, false);
		document.addEventListener("touchstart", touchstartHandler, false);
		document.addEventListener("touchend", touchendHandler, false);
	}else if( navigator.userAgent.indexOf('Android') != -1 )
	{
		$(".popwrap").css({"min-height": "100%"});
		$("#popcontents-footer").css( {"position": "absolute", "bottom": "23px"} );
		$("#footerpop").css( {"position": "absolute", "bottom": "0"} );
	}
	
	$("#nebuta-contents").css({"visibility": "visible"});
	
	//-------------------------------------------------
	//<p id="goto-top"><a href="#">ページのトップへ</a></p>
	$("#goto-top").click(function(){ $("html,body").animate({ scrollTop: 0 }, 750, "easeOutCirc"); return false; });
	
	onResize();
	//リサイズしたら実行
	$(window).resize(function(){
		onResize();
	});
	$(window).scroll(function(){ onScroll(); });
	
	

	
})

//導入アニメーション
///////////////////////////////////////////////
function play(_skip)
{
	//-------------------------------------------------
	if( _skip )
	{
		$("#main-visual").css({"display": "none"});
		$("#contents-body").css({"display": "block"});
		$("html").css({"overflow": "auto"});
		$("#contents-body-inner").animate({"opacity": "1"}, 500, "easeInCirc");
		var mr = $("#contents-body").css("padding-top");
		$("#contents-body").css({"padding-top": 400});
		$("#contents-body").animate({"padding-top": 72},2000, "easeOutExpo", function(){
			if(navigator.userAgent.indexOf('iPhone') != -1 || navigator.userAgent.indexOf('iPad') != -1){
				$(".popwrap").css({ "height": $("#nebuta-container-wrapper").height() + 128 });
				winH = $(window).height();
				
			}
			
			for( var i = 0; i < youtube_list.length; i++ ){
				var w = youtube_list[i].width();
				var h = youtube_list[i].height();
				var url = "http://www.youtube.com/v/";
				var youtube_id = youtube_list[i].attr("id");
				if( navigator.userAgent.indexOf('iPhone') != -1 || navigator.userAgent.indexOf('iPad') != -1 || navigator.userAgent.indexOf('Android') != -1 ){
					url = "http://www.youtube.com/embed/";
					youtube_list[i].append("<iframe width='" + w + "' height='" + h + "' src='" + url + youtube_id +  "?showinfo=0&wmode=transparent&rel=0&hd=1' frameborder='0' allowfullscreen></iframe>");
				}else {
					youtube_list[i].append("<object width='" + w +"' height='" + h + "'><param name='movie' value='http://www.youtube.com/v/" + youtube_id + "?rel=0&hd=1&showinfo=0'></param><param name='allowFullScreen' value='true'></param><param name='allowscriptaccess' value='always'></param><param name='wmode' value='transparent'></param><embed src='http://www.youtube.com/v/" + youtube_id + "?rel=0&hd=1&showinfo=0' type='application/x-shockwave-flash' width='" + w + "' height='" + h + "' allowscriptaccess='always' wmode='transparent' allowfullscreen='true'></embed></object>");
				}
				
			}
				
		});
		
	}else{
		$("#main-visual-img").css({"opacity": "0"});
		$("#main-visual").css({"display": "block"});
		
			
			
			
		$("#main-visual-img").stop().delay(500).animate({opacity: 1}, 500);
			$("#background-shadow").stop().delay(500).animate({opacity: 0}, 500, "easeInQuart");
			$("#background-light").stop().delay(500).animate({opacity: 0.4}, 500, function(){
				$("#background-light").animate({"opacity": 0}, 500);
			});
			
				$("#main-visual").stop().delay(1500).animate({"opacity": 0}, 1000, "easeInCirc", function(){
					$("#main-visual").css({"display": "none"});
				});
			
				$("#contents-body-inner").stop().delay(2350).animate({"opacity": "1"}, 500, "easeInCirc");
					$("#contents-body-inner").delay(5000, function(){
						$("#contents-body").css({"display": "block"});
						$("html").css({"overflow": "auto"});
					});
			
			var mr = $("#contents-body").css("padding-top");
			$("#contents-body").css({"padding-top": 400});
				$("#contents-body").stop().delay(2000).animate({"padding-top": 72},2000, "easeOutCubic", function(){
					if(navigator.userAgent.indexOf('iPhone') != -1 || navigator.userAgent.indexOf('iPad') != -1){
						$(".popwrap").css({ "height": $("#nebuta-container-wrapper").height() + 128 });
						winH = $(window).height();
					}
					
					for( var i = 0; i < youtube_list.length; i++ ){
						var w = youtube_list[i].width();
						var h = youtube_list[i].height();
						var url = "http://www.youtube.com/v/";
						var youtube_id = youtube_list[i].attr("id");
						if( navigator.userAgent.indexOf('iPhone') != -1 || navigator.userAgent.indexOf('iPad') != -1 || navigator.userAgent.indexOf('Android') != -1 ){
							url = "http://www.youtube.com/embed/";
							youtube_list[i].append("<iframe width='" + w + "' height='" + h + "' src='" + url + youtube_id +  "?showinfo=0&wmode=transparent&rel=0&hd=1' frameborder='0' allowfullscreen></iframe>");
						}else {
							youtube_list[i].append("<object width='" + w +"' height='" + h + "'><param name='movie' value='http://www.youtube.com/v/" + youtube_id + "?rel=0&hd=1&showinfo=0'></param><param name='allowFullScreen' value='true'></param><param name='allowscriptaccess' value='always'></param><param name='wmode' value='transparent'></param><embed src='http://www.youtube.com/v/" + youtube_id + "?rel=0&hd=1&showinfo=0' type='application/x-shockwave-flash' width='" + w + "' height='" + h + "' allowscriptaccess='always' wmode='transparent' allowfullscreen='true'></embed></object>");
						}
						
					}
				});

	}
}


///////////////////////////////////////////////
function onResize()
{
	if( $(window).width() < 1024 ){
		$("#popcontents-footer").css({ "width": "1024px" });
		$("#footerpop").css({ "width": "1024px" });
		
	}else{
		$("#popcontents-footer").css({ "width": "100%" });
		$("#footerpop").css({ "width": "100%" });
	}
	if( navigator.userAgent.indexOf('Android') != -1 ){
		$(".popwrap").css({"height": $("#contents-body-inner").height() + 128 + 23 + 35 + 68});
	}

	onMainResize();
	onScroll();

}

//表示時のメインビジュアルをリサイズ
///////////////////////////////////////////////
function onMainResize()
{
	
	//画像サイズ指定
	var imgW = 1009;
	var imgH = 560;
	
	//ウィンドウサイズ取得
	var winW = $(window).width();
	var winH = $(window).height();
	
	//コンテンツエリアをリサイズ
	$("#bg").css({"overflow": "hidden"});

	var scaleW = winW / imgW;
	var scaleH = (winH - 130) / imgH;
	var fixScale = Math.max(scaleW, scaleH);
	
	var setW = imgW * fixScale;
	var setH = imgH * fixScale;
	
	bakcgroundResize(setW, setH);
}
///////////////////////////////////////////////
function bakcgroundResize(w, h)
{
	$("#main-visual img").css({"width": w, "height": h});
	$("#main-visual").css({"width": w, "height": h});
	$("#background-shadow").css({"width": w, "height": h});
	$("#background-light").css({"width": w, "height": h});
}

function onScroll(){

	if(navigator.userAgent.indexOf('iPhone') != -1 || navigator.userAgent.indexOf('iPad') != -1 || navigator.userAgent.indexOf('Android') != -1) return;
	
	var left = -$(window).scrollLeft();
	$("#popcontents-footer").css({ "left": left });
	$("#footerpop").css({ "left": left });

}

//iPhone/iPad
///////////////////////////////////////////////
function touchHandler(e)
{
//	e.preventDefault();
	$("#popcontents-footer").css( {"opacity": "0" } );
	$("#footerpop").css({"opacity": "0" });
}

function touchstartHandler(e)
{
	$("#popcontents-footer").css( {"opacity": "0" } );
	$("#footerpop").css({"opacity": "0" });
}
var winH;
function touchendHandler(e)
{
/*	$("#popcontents-footer").css( {"display": "block", "top": winH + $(window).scrollTop() - $("#popcontents-footer").height() - $("#footerpop").height() } );
	$("#footerpop").css({"display": "block",  "top": winH + $(window).scrollTop() - $("#footerpop").height() });
	$("#popcontents-footer").css( {"display": "block", "bottom": $("#footerpop").height() } );
	$("#footerpop").css({"display": "block",  "bottom": "0" });
	
	$("#popcontents-footer").css( {"display": "block", "top": winH + window.pageYOffset - $("#popcontents-footer").height() - $("#footerpop").height() } );
	$("#footerpop").css({"display": "block",  "top": winH + window.pageYOffset - $("#footerpop").height() });
	
	alert(window.pageYOffset);*/
	
/*	$("#popcontents-footer").css( {"display": "block", "top": window.pageYOffset + window.innerHeight - $("#popcontents-footer").height() - $("#footerpop").height() } );
	$("#footerpop").css({"display": "block", "top": window.pageYOffset + window.innerHeight - $("#footerpop").height() });*/
	
	$("#popcontents-footer").animate({"opacity": 1}, 1000);
	$("#footerpop").animate({"opacity": 1}, 1000);
	$("#popcontents-footer").css( {"top": window.pageYOffset + window.innerHeight - $("#popcontents-footer").height() - $("#footerpop").height() } );
	$("#footerpop").css({"top": window.pageYOffset + window.innerHeight - $("#footerpop").height() });
}

function tmp()
{
	$("#popcontents-footer").css( {"top": window.pageYOffset + window.innerHeight - $("#popcontents-footer").height() - $("#footerpop").height() } );
	$("#footerpop").css({"top": window.pageYOffset + window.innerHeight - $("#footerpop").height() });
}