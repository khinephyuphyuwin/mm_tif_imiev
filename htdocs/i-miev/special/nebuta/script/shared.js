////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//   roll over   //
//////////////////
// 
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
$(function(){
	
	//スマートフォンは処理なし
	if(navigator.userAgent.indexOf('iPhone') != -1 || navigator.userAgent.indexOf('Android') != -1 || navigator.userAgent.indexOf('iPad') != -1) return;
	$.fn.tipsy.defaults = {
		delayIn: 0.1,      // delay before showing tooltip (ms)
		delayOut: 0,     // delay before hiding tooltip (ms)
		fade: false,     // fade tooltips in/out?
		fallback: '',    // fallback text to use when no tooltip text
		gravity: 'sw',    // gravity
		html: false,     // is tooltip content HTML?
		live: false,     // use live event support?
		offset: 7,       // pixel offset of tooltip from element
		opacity: 1,    // opacity of tooltip
		title: 'title',  // attribute/callback containing tooltip text
		trigger: 'hover' // how tooltip is triggered - hover | focus | manual
	};
	
	$("#top a").tipsy({
		live: true,
		gravity: 'sw',
		trigger: 'hover',
		offset: 6
	});
	
	$("#about a").tipsy({
		live: true,
		gravity: 'sw',
		trigger: 'hover',
		offset: 8
	});
	$("#plan a").tipsy({
		live: true,
		gravity: 'sw',
		trigger: 'hover',
		offset: 8
	});
	$("#report a").tipsy({
		live: true,
		gravity: 'sw',
		trigger: 'hover',
		offset: 8
	});
	$("#artist a").tipsy({
		live: true,
		gravity: 'sw',
		trigger: 'hover',
		offset: 8
	});
	$("#system a").tipsy({
		live: true,
		gravity: 'sw',
		trigger: 'hover',
		offset: 8
	});
	$("#extramovie a").tipsy({
		live: true,
		gravity: 'sw',
		trigger: 'hover',
		offset: 8
	});
	
	//footer
	$(".comming-soon").each(function(){
		var id = $(this).attr("id");
		$(this).css({"cursor": "hand"});
		$(this).bind("mouseover", function(){ $(this).attr("id", id+"-over") });
		$(this).bind("mouseout", function(){ $(this).attr("id", id) });
	});
})

///////////////////////////////////////////////

