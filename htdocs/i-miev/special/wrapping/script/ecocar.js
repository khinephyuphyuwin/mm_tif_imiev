var stayflag = new Array();

var foldsystem = {};

foldsystem.foldstart = function(){
	
	if(document.getElementById('foldArea')) foldsystem.foldarea = document.getElementById('foldArea');
	else return;
	//alert(document.getElementById('foldArea'));
	foldsystem.foldarea.style.display = 'none';
	
	foldsystem.buttonElm = document.getElementById('foldbt');
	foldsystem.buttonElm.style.display = 'block'
	
	foldsystem.buttonImg = foldsystem.buttonElm.getElementsByTagName('img')[0];
	foldsystem.buttonImg.style.cursor = 'pointer';
	
	addListener(foldsystem.buttonImg, 'click', foldsystem.foldset);
	
}

foldsystem.foldset = function (e){
	stopbubble(e);
	var foldbox = foldsystem.foldarea.style.display;
	
	if(foldbox == '' || foldbox == 'none'){
		foldsystem.foldarea.style.display = 'block';
		foldsystem.buttonImg.src = 'images/ecocar_bt_01.gif';
		foldsystem.buttonImg.alt = '環境対応車普及促進税制についてを閉じる';
	}else{
		foldsystem.foldarea.style.display = 'none';
		foldsystem.buttonImg.src = 'images/ecocar_bt_01_a.gif';
		foldsystem.buttonImg.alt = '環境対応車普及促進税制について';
	}
}

DOMready.tgFunc(foldsystem.foldstart);
//addListener(window, 'load', foldsystem.foldstart);

